# Tests to cover basic functionality and assertions in pytest playwright


def test_verify_url(page):
    page.goto("/")
    page.click("text=Dynamic ID")
    assert page.url == "http://www.uitestingplayground.com/dynamicid"


def test_verify_text(page):
    page.goto("/")
    # Page title has a line break in, which shows as no space rather than \n
    assert page.text_content("h1") == "UI Test AutomationPlayground"


def test_click_colour(page):
    page.goto("/click")
    page.click("#badButton")
    assert page.is_visible(
        "//button[contains(concat(' ',normalize-space(@class),' '),' btn-success ')]"
    )


def test_basic_login(page):
    # This login app does not redirect on login, and instead provides screen text
    page.goto("/sampleapp")
    page.fill("//input[@placeholder='User Name']", "user")
    page.fill("//input[@name='Password']", "pwd")
    page.click("#login")
    assert page.inner_text("#loginstatus") == "Welcome, user!"


def test_fail_login(page):
    page.goto("/sampleapp")
    page.fill("//input[@name='Password']", "pwd")
    page.click("#login")
    assert page.inner_text("#loginstatus") == "Invalid username/password"


def test_basic_logout(page):
    page.goto("/sampleapp")
    page.fill("//input[@placeholder='User Name']", "foo")
    page.fill("//input[@name='Password']", "pwd")
    page.click("#login")

    page.click("#login")
    assert page.inner_text("#loginstatus") == "User logged out."
