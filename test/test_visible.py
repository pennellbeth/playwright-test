# Tests to cover selecting obscured elements in Playwright


def test_visibility_b4(page):
    page.goto("/visibility")
    assert page.is_visible("#hideButton")
    assert page.is_visible("#removedButton")
    assert page.is_visible("#zeroWidthButton")
    assert page.is_visible("#overlappedButton")
    assert page.is_visible("#transparentButton")
    assert page.is_visible("#invisibleButton")
    assert page.is_visible("#notdisplayedButton")
    assert page.is_visible("#offscreenButton")


def test_visibility_aftr(page):
    # Helpfully, Playwright treats any element as invisible with:
    #  - An empty bounding box
    #  - Elements of zero size
    #  - 'visibility:hidden' computed style
    #  - 'display:none' computer style
    # So we can re-use the is_hidden function rather than writing new assertions
    page.goto("/visibility")
    page.click("#hideButton")
    assert page.is_hidden("#removedButton")
    assert page.is_hidden("#zeroWidthButton")
    # The overlapped element isn't invisible, so we assert the covering layer is visible
    assert page.is_visible("#hidingLayer")
    attr = page.get_attribute("#transparentButton", "style")
    assert attr == "opacity: 0;"
    assert page.is_hidden("#invisibleButton")
    assert page.is_hidden("#notdisplayedButton")
    # Id is still visible, so we need to assert on the change in unique class
    assert page.is_visible(
        "//button[contains(concat(' ',normalize-space(@class),' '),' offscreen ')]"
    )


def test_hidden_layers(page):
    # Green button overlapped by blue button on click
    # It prints to the console which colour buttons have been pressed
    page.goto("/hiddenlayers")
    page.on("console", lambda msg: print(msg.text))

    with page.expect_console_message() as msg_info:
        page.locator("div[class='spa-view']").locator("button").click()
    assert str(msg_info.value) == "Green button pressed"
    # TODO: Add predicate to ensure context doesn't resolve on the first message handled
    with page.expect_console_message() as msg_info2:
        buttons = page.locator("div[class='spa-view']")
        buttons.locator("button").last.click()
    assert str(msg_info2.value) == "Blue button pressed"
