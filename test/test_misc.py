# Tests to cover other UI features that can crop up for automation


def test_progress_bar(page):
    # TODO: Find a less flakey way to solve this test.
    # This is trickier since the speed of the progress bar is random. 
    # So if it's too quick, the Result field can miss the 75 value by nanoseconds and 
    # this fails the test. Or, if it's really slow, the test itself can time out.
    page.goto("/progressbar")
    page.click("#startButton")
    page.inner_text("#progressBar[aria-valuenow='75']")
    page.click("#stopButton")
    assert "Result: 0" in page.inner_text("#result")


def test_text_input(page):
    page.goto("/textinput")
    page.fill("#newButtonName", "It's-a-me, Mario!")
    page.click("#updatingButton")
    assert page.inner_text("#updatingButton") == "It's-a-me, Mario!"
