# Tests to cover common challenges with UI selectors in Playwright


def test_ajax_data(page):
    # Test needs to wait 15 secs for text to appear
    page.goto("/ajax")
    page.click("#ajaxButton")
    content = page.text_content(".bg-success")
    assert content == "Data loaded with AJAX get request."


def test_client_side_delay(page):
    # Test needs to wait 15 secs for text to appear
    page.goto("/clientdelay")
    page.click("#ajaxButton")
    assert page.text_content(".bg-success") == "Data calculated on the client side."


def test_dynamic_table(page):
    # Table design has no unique IDs, just divs and spans
    page.goto("/dynamictable")

    # Find the table
    table = page.locator("div[aria-label='Tasks']")

    # Find the horizontal index of CPU header
    headers = table.locator("span[role='columnheader']")
    cpu_col_index = headers.all_inner_texts().index("CPU")

    # Find the values of the Chrome row
    rows = table.locator("div[role='rowgroup']")
    table_items = rows.nth(1)
    chrome_element = table_items.locator("span[role='cell']").locator("text=Chrome")
    chrome_row = chrome_element.locator("xpath=..").locator("span[role='cell']")
    chrome_cpu = chrome_row.nth(cpu_col_index).inner_text()

    # Now to compare the values
    assert page.locator(".bg-warning").inner_text().endswith(chrome_cpu)
