This mini-project is an experimentational dive into Playwright Python, and the pytest plugin for Playwright.
It utilises the following online UI automation sites:
  - http://www.uitestingplayground.com/

Helpfully, the pytest plugin handles a lot of Playwright features for us, including default browser, context and page management 
and puts a load more into CLI commands (for example video of tests and slow-mo mode).

The `pytest.ini` file contains the default settings for running our Playwright tests.
Further options such as device emulation and slow-mo mode are in the pytest help menu.

So you can start running the tests in the repo by simply running `pytest`.
